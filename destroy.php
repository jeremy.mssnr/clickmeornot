<?php 
//-------------------------------------------------------------------------------------------
// Examen de module M133 - Janvier 2016
// Auteurs : Pascal Bonvin / Pascal Comminot
// Fichier : logout.php
// Description : script de déconnexion du site
// Version 1.0 : PB / version initiale
// Version 1.1 : PC / remise en forme du code

// Suppression de la session selon les indications de php.net / session_destroy
// démarrage de la session
session_start();

// suppression des données de la session
$_SESSION = array();

// suppression du cookie de session
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
// suppression de la session
session_destroy();

// redirection sur la page index
header("Location: ./index.php");
