<?php
/*
 *  Auteur      : Doriane Pott & Brian Golay & Jeremy Meissner & Romain Camphana
 *  Classe      : I.FDA P2D
 *  Date        : 16/04/2019
 * Description  : ...
      page d'accueil
      attention!
      l'utilisateur n'a pour identifiant que le nom,
      il n'a pas de mot de passe!
*/
require_once("./sql.php");
session_start();

// si la session n'existe pas, alors on la créée
if(!isset($_SESSION['user_log'])){
  $_SESSION['user_log'] = NULL;
}

// si on a déjà des données stockées dans le session, alors on les garde en local dans le code.
if (($_SESSION["user_log"] || $_SESSION['user_log'] != "") && $_SESSION['game'] != ""){
  $nickname = $_SESSION['user_log'];
}
else{
  // on initialise les données :
  $_SESSION['user_log'] = NULL;
  $nickname = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
}

$submit = filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING);
$errormsg = array();

// Traitement des erreurs
if(strlen($nickname) > 20 || strlen($nickname) == 0 || $nickname == ""){
  if (strlen($nickname) > 20){
    array_push($errormsg, "Maximum de caractères autorisés dépassé! (max. 20) ");
  }
  if (strlen($nickname) == 0 || $nickname == ""){
    array_push($errormsg, "Veuillez entrer un nom d'utilisateur!! ");
  }
}
else{
  $errormsg = array();
}

// si tout se passe bien :
if($errormsg == array()){
  // sauvegarde des données dans la session :
  $_SESSION['game'] = $submit;
  $_SESSION['user_log'] = $nickname;
}

// en fonction du jeu sélectionné, on le guide sur la page adaptée
if($nickname != NULL && $submit == "dactylo" ) {
  add_User($nickname);
  header('Location: ./game.php');
  exit;
}
else if($nickname != NULL && $submit == "reflex") {
  add_User($nickname);
  header('Location: ./gameclick.php');
  exit;
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Accueil</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' href='./css/style.css'>
    <script src='main.js'></script>
</head>
<body>
<form method="post" action="#">
      <article>
        <h1>Click me or not</h1>
        <p>
          <label for="id">YOUR USERNAME : </label><br>
          <input type="text" name="id" placeholder="John Doe"/>
        </p>
        <div>
          <button type="submit" name="submit" value="reflex">Go to reflex game</button>
          <button type="submit" name="submit" value="dactylo">Typist game</button>
        </div>
        <span>Created by the best team : Brian - Doriane - Jeremy - Romain / Error :
	      	<?php
            // Affichage du message d'erreur :
            foreach($errormsg as $msg){
                echo $msg;
            }
          ?>
        </span>
      </article>
    </form>
</body>
</html>
