<?php
/*
 *  AUTEURS       :   BRIAN GOLAY & DORIANE POTT
 *  CLASSE        :   I.FDA P2D
 *  DATE          :   07 Mai 2019
 *  DESCRIPTION   :
 *            Ensemble des fonctions permettant de trouver des données
              qui sont dans la base et les retourner.
              Ou enregistrer de nouvelles entrées dans la base.
 *  /!\           :   ce code contient des bugs
 62.210.144.144
*/
function connect() {
    static $myDb = null;
    $dbName = "clickmeornot";
    $dbUser = "myroot2";
    $dbPass = "root";
    if ($myDb === NULL) {
        try {
            $myDb = new PDO(
                    "mysql:host=localhost;dbname=$dbName;charset=utf8", $dbUser, $dbPass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false)
            );
        } catch (Exception $e) {
            die("Impossible de se connecter à la base " . $e->getMessage());
        }
    }
    return $myDb;
}

// fonction d'ajout d'utilisateur :
function add_User($nickname){
    // si pas dans la base :
      $sql = "INSERT INTO `t_joueur` (`Pseudo`) VALUES (:pseudo) ON DUPLICATE KEY UPDATE `Pseudo` = `Pseudo`";
      $query = connect()->prepare($sql);

      $query->execute([
        ':pseudo' => $nickname,
      ]);
}
