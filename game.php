<?php
/**
 * AUTEUR       :       Doriane Pott Jeremy Meissner
 * CLASSE       :       I.FDA P2D
 * DATE         :       14 mai 2019
 * DESCRIPTION  :       Page de jeu.
**/
session_start();

if (isset($_SESSION['user_log'])) {
    $username   = $_SESSION['user_log'];
    $game       = $_SESSION['game'];
}

?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
  .blocus {
  user-select: none;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
}
  </style>
  <body onload="bloc1(), alert_keycode()">
    <article id='sortie' class='blocus'></article>
    <article class="leaderboard">
    </article>
    <script type='text/javascript' src='dactylo.js'></script>
    <input type="text" id="texte" onkeydown="check_enter_char(event)"cols="40"
       rows="5">
    <button id="check" class="check" onclick="check()">Fini!</button>
    <article id="dis"></article>
  </body>
</html>
