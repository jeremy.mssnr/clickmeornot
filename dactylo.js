/*
 *  Auteur      : Doriane Pott & Brian Golay
 *  Classe      : I.FDA P2D
 *  Date        : 14/05/2019
 * Description  : ...
      Code concernant le jeu de dactylographie.

      CE CODE CONTIENT DES BUGS!

      Abandon de l'option de supression d'accents.
 */


// fonction d'initialisation des données :
var sortie = document.getElementById("sortie");
var texte_a_taper = new Array();
var texte_entre = new Array();
var compteur = 0;
var time = 0;

function generate() {
  window.setInterval("timer()", 100);
}
generate();
function timer() {
  time += 100;
}



texte_a_taper[0] = "Ne pas ceder face a la pluie." + " Ne pas ceder face au vent." +
  " Ne pas ceder non plus face a la neige ou a la chaleur de l’ete." + " Avec un corps solide Sans avidite." +
  " Sans perdre son temperament." + " Cultivant une joie tranquille." + " Chaque jour quatre bols de riz complet." +
  " Du miso et un peu de legumes a manger." + " Dans toutes les choses." + " Sans y mettre ses emotions." + " Voir, ecouter et comprendre" +
  " Et sans oublier." + " Dans l’ombre des bois de pin des champs." + " Vivre dans une cabane au toit de chaume." +
  " S’il y a un enfant malade a l’Est." + " Y aller et le veiller." + " S’il y a une mere fatiguee a l’Ouest." +
  " Y aller et porter sa gerbe de riz." + " S’il y a quelqu’un proche de la mort au Sud." +
  " Y aller et lui dire qu’il n’y a pas besoin d’être effraye." + " S’il y a une dispute ou un litige au Nord." + " Leur dire de ne pas perdre leur temps en actes inutiles." + " En cas de secheresse, verser ses larmes de sympathie." +
  " Lors d’un ete froid, errer bouleverse." + " Appele bon a rien par tout le monde." + " Sans être complimente." +
  " Ni rendu responsable." + " Une telle personne." + " Je voudrais devenir.";

texte_a_taper[1] = "Chaque ville possede son odeur." +
  " Celle de Florence est faite d'un melange d'iris blanc, de poussiere, de brume et du vernis d'antiques tableaux (Meljukovski)." +
  " Si l'on me demandait quelle est l'odeur de Tokyo, je repondrais sans la moindre hesitation : l'eau du Fleuve." +
  " Il ne s'agit pas seulement de son odeur : sa couleur, ses resonances, ne peuvent être que la couleur, la rumeur de ce Tokyo que j'aime." +
  " C'est parce que le fleuve existe que j'aime Tokyo ;" +
  " c'est parce que Tokyo existe que j'aime la vie.";

texte_a_taper[2] = "Chassez-les ! Chassez-les tous !" +
  " Ceux qui dechainent le chaos juste pour assouvir leur soif d’alcool." +
  " Ceux qui se livrent a tous les exces pour satisfaire leurs propres desirs." +
  " Mais qui professent que la vie n’est qu’un desert." +
  " Ceux qui ne peuvent vivre sans se comparer aux autres." +
  " Jetez-les a terre, ecrasez-les tous." +
  " Exorcisez les demons lâches et peureux qu’ils cachent en eux." +
  " Et donnez-les en pâture aux poissons et aux cochons." +
  " Tout comme on forge l’acier, une generation nouvelle forge des hommes nouveaux." +
  " Brisez les arêtes des pays montagneux couleur de nuit." +
  " Et faites de la voie lactee votre centrale electrique.";

texte_a_taper[3] = "Dans ce cosmos imaginaire immense et mysterieux, si, brulant d’un juste desir, on cherche a atteindre le bonheur parfait avec les autres et tout chose en cet univers, si l’ont fait de cela sa religion, ce desir nous laissera brises ou, uses, nous l’abandonnerions et c’est avec une seule autre âme que cherchera a vivre, parfaitement et pour l’eternite."
" Cette alteration s’appelle amour." +
  " (extrait de 'Printemps et Ashura')";

texte_a_taper[4] = "ecoutez bien : " +
  "Les etoiles " +
  "Sont les fleurs du ciel " +
  "Les fleurs " +
  "Les etoiles de ce monde " +
  "dit-on.";

let rnd = 0;
rnd = Math.floor((Math.random() * texte_a_taper.length - 1) + 1);
var txtrnd = texte_a_taper[rnd];

// CTRL & ALT disabled
function alert_keycode() {
  // Desactiver ctrl et alt
  if (event.keyCode == 17) {
    alert("VOLEUR : on ne copie pas s'il-vous-plait");
  }
}

function bloc1() {
  sortie.innerHTML = "<h1>Veuillez retaper le texte suivant : </h1> <p id='juste'>" + txtrnd + "</p>";
}


function check() {
  var saisie = document.getElementById("texte");
  var dyslexie = document.getElementById("dis");
  var bon = document.getElementById("juste")
  if (saisie.value != txtrnd) {

    dyslexie.innerHTML = "Le texte saisi n'est pas correct!"
  } else {
    clicked();
  }
}

function check_enter_char(event) {
  console.log(event.keyCode);
  console.log(event.key);
  // PROBLeME AVEC LA COMAPRAISON DES DEUX CHAiNES (celle entree, et la vreitable)
  // ON NE COMPREND CAR LES DEUX VARIABLE a LA POSITION 'I' ONT LA MÊME VALEUR POURTANT!!!!!!!!!

  document.onkeydown = alert_keycode();

  var saisie = document.getElementById("texte");
  var dyslexie = document.getElementById("dis");
  var char_entre = event;

  if (event.code[0] != 'K') {
    if (char_entre == "Backspace") {
      console.log("EFFACER");
    }
    else if (event.keyCode == "13") {
      char_entre = "<br>";
      console.log("espace");
    }
    else {
      console.log("caratère inconnu");
    }
  }
  else {  // Si c'est bien une lettre
    if (char_entre == txtrnd[compteur]) {
      saisie.style.color = 'green';
    }
    else {
      saisie.style.color = 'red';
    }
  }
  console.log("Char : " + char_entre + " Texte : " + txtrnd[compteur] + " Compteur de mort : " + compteur);
}

function clicked() {
  console.log(time);
  let result = time;
  alert("Bien joué vous avez mis " + result / 1000 + "secondes");
  window.location = "./index.php";
}
