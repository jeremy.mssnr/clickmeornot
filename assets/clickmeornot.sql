-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 30 Avril 2019 à 15:25
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `clickmeornot`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_jeux`
--

CREATE TABLE IF NOT EXISTS `t_jeux` (
  `id_Jeux` int(11) NOT NULL AUTO_INCREMENT,
  `nomJeux` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Jeux`),
  UNIQUE KEY `id_Jeux` (`id_Jeux`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_jeuxjoueur`
--

CREATE TABLE IF NOT EXISTS `t_jeuxjoueur` (
  `id_Jeux` int(11) NOT NULL,
  `Pseudo` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id_Jeux`,`Pseudo`),
  UNIQUE KEY `id_Jeux` (`id_Jeux`),
  KEY `Pseudo` (`Pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_joueur`
--

CREATE TABLE IF NOT EXISTS `t_joueur` (
  `Pseudo` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`Pseudo`),
  UNIQUE KEY `Pseudo` (`Pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_record`
--

CREATE TABLE IF NOT EXISTS `t_record` (
  `id_Record` int(11) NOT NULL AUTO_INCREMENT,
  `record` int(11) NOT NULL,
  `Pseudo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_Record`,`Pseudo`),
  UNIQUE KEY `id_Record` (`id_Record`),
  KEY `Pseudo` (`Pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_jeuxjoueur`
--
ALTER TABLE `t_jeuxjoueur`
  ADD CONSTRAINT `t_jeuxjoueur_ibfk_2` FOREIGN KEY (`Pseudo`) REFERENCES `t_joueur` (`Pseudo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `t_jeuxjoueur_ibfk_1` FOREIGN KEY (`id_Jeux`) REFERENCES `t_jeux` (`id_Jeux`);

--
-- Contraintes pour la table `t_record`
--
ALTER TABLE `t_record`
  ADD CONSTRAINT `t_record_ibfk_1` FOREIGN KEY (`Pseudo`) REFERENCES `t_joueur` (`Pseudo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
